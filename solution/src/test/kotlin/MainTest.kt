package org.bsc.katas

import kotlin.test.Test
import kotlin.test.assertEquals

class MainTest {

    private val rover = Main()


    @Test
    fun `when starting out verify the environment works`() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun `api works`() {
        assertEquals("0:0:N", rover.execute(""))
    }

    @Test
    fun `test a move`() {
        assertEquals("0:1:N", rover.execute("M"))
    }

    @Test
    fun `test another move`() {
        assertEquals("0:2:N", rover.execute("MM"))
    }

    @Test
    fun `test wrap drive`() {
        assertEquals("0:0:N", rover.execute("MMMMMMMMMM"))
    }

    @Test
    fun `test turn right`() {
        assertEquals("0:0:E", rover.execute("R"))
    }

    @Test
    fun `test turn left`() {
        assertEquals("0:0:W", rover.execute("L"))
    }

    @Test
    fun `test moving to position 1 1`() {
        assertEquals("1:1:N", rover.execute("RMLM"))
    }

    @Test
    fun `test move to position 3 3 then spin around left`() {
        assertEquals("3:3:N", rover.execute("RMLMRMLMRMLMRRRRLLLLRRLLRRRRLLLL"))
    }


    @Test
    fun `test not rotating when facing north`() {

        val d = Main.Direction(0, 1)
        val newD = d.applyOp(rover.identity)

        assertEquals(d, newD)

    }

    @Test
    fun `test not rotating when facing west`() {

        val d = Main.Direction(-1, 0)
        val newD = d.applyOp(rover.identity)

        assertEquals(d, newD)

    }


    @Test
    fun `when facing north then turning right should face east`() {

        val newD = rover.north.applyOp(rover.right)
        assertEquals(rover.east, newD)

    }

    @Test
    fun `when facing north then turning left should face west`() {

        val newD = rover.north.applyOp(rover.left)
        assertEquals(rover.west, newD)

    }

    @Test
    fun `when facing north then turning left two times should face south`() {

        val newD = rover.north
            .applyOp(rover.left)
            .applyOp(rover.left)

        assertEquals(rover.south, newD)

    }


    @Test
    fun `when facing north then turning left four times should still face north`() {

        val newD = rover.north
            .applyOp(rover.left)
            .applyOp(rover.left)
            .applyOp(rover.left)
            .applyOp(rover.left)

        assertEquals(rover.north, newD)

    }


}
