package org.bsc.katas

class Main {


    /*
        A B
        C D
     */
    data class Operator(
        val a: Int,
        val b: Int,
        val c: Int,
        val d: Int
    )


    // Stolen from here:  https://en.wikipedia.org/wiki/Rotation_matrix
    // 90° = π/2   Cos(π/2) = 0   Sin(π/2) = 1
    val left = Operator(
        0, -1,
        1, 0
    )

    val right = Operator(
        0, 1,
        -1, 0
    )


    val identity = Operator(
        1, 0,
        0, 1
    )


    data class Direction(val x: Int, val y: Int) {
        fun applyOp(op: Operator) = Direction(
            op.a * x + op.b * y,
            op.c * x + op.d * y
        )
    }

    val north = Direction(0, 1)
    val south = Direction(0, -1)
    val east = Direction(1, 0)
    val west = Direction(-1, 0)


    private val dirMap = mapOf(
        north to 'N',
        south to 'S',
        east to 'E',
        west to 'W'
    )


    var y: Int = 0
    var x: Int = 0
    var d = Direction(0, 1)


    fun execute(s: String): String {
        for (c: Char in s) {
            when (c) {
                'M' -> {
                    x += d.x;
                    y += d.y
                }
                'R' -> d = d.applyOp(right)
                'L' -> d = d.applyOp(left)
            }
            y %= 10
        }
        return "$x:$y:${dirMap[d]}"
    }


}
