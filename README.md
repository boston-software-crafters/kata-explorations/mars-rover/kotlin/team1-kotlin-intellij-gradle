# Mars Rover Kata April 13th Boston Software Crafters exploration

This project is an exploration developed for the monthly [Boston Software Crafters](https://boston-software-crafters.github.io/website/) meet-up event using Kotlin with IntelliJ and Gradle.

The most relevant and interesting information is the [Mars Rover Kata overview](MarsRover.md) where you will see the rules and goals for the kata.

Next in importance is a short discussion of [using TDD on this kata](TDD.md).

Note [the basic requirements](Prerequisites.md) for working on this kata with Kotin, IntelliJ and Gradle.

The rest of this document is for notes to be added during the event by the mobbers.

## Team1 Members

*  Paul Reilly (facilitator)
*  vlad
*  Mark Pralat (observer)
*  Michael Tomlinson (observer)

## Retrospective

This was the second straight Kotlin intro session.
I did the whole session as Driver and Vlad as navigator.
We both enjoyed the exercise and look forward to the next session.

I was fearful that turning the remote control to another participant would break in ways that I experienced with Danil.
Based on that experience, I am planning on doing the next session (May) using VSCode/LiveShare.
My goal is to have this feature ready in the next two weeks.

As a general note about the event: the session join/leave/join/leave/join/leave approach was painful (seamy). Breakout rooms is the answer to this issue.
My goal is to master this feature in the next couple of weeks as well. 