## Challenges [optional]

If you are already in the Internet Hall Of Fame and have finished this kata before everyone else has read the README.md file, then here are some challenges for you:

1) Create a functional programming style solution if you have already done an OO solution. Or vice-versa. Which solution is better? faster?

2) Make your solution clean, as in the principles and practices of Robert "Uncle Bob" Martin and his Clean Code world: use good names, short functions, good structure, SOLID code, etc.
